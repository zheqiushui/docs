# Global Tooks configuration
Install Maven - Auto  
Install Docker - Auto

#Jenkins Job DSL
script for Jenkins job setup without Jenkins UI step by step config. 
in normal, file name like job.groovy
once loading the file from somewhere(git/local disk), it will auto do 
UI config. 

#dial unix /var/run/docker.sock: connect: `permission denied`
#This issue occurred when build and push docker image in Jenkins job
1. 将当前用户加入到docker组中
   sudo gpasswd -a 用户名 用户组 ---> sudo gpasswd -a go docker
   表示当前用户: $USER  
2. 将当前用户切换到docker组中  
   newgrp - docker  

## Seems above solution is not work, then run below
Go to /var/run  
chmod 666 docker.sock  
and restart jenkins  

# Docker Jenkins Plugin for build/push image to repo
Plugin name: CloudBees Docker Build and Publish plugin

# Do setup for Docker build and publish
In 'add build' option list, choose 'Docker Build and Publish'.
Provide below parameters  
a. Repository Name (e.g zheqiushui/skybook. default is dockerhub)  
b. Tag (e.g v1.1)  
c. Registry Credentials (e.g dockerhub login with token)  
