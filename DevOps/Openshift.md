# For Windows
## Download VirtualBox
install it on Windows
## Download minishift from GitHub
### GitHub: https://github.com/minishift/minishift/releases/tag/v1.34.3
install it with command  
minishift.exe start --vm-driver virtualbox  
### Starting log
-- Starting profile 'minishift'  
-- Check if deprecated options are used ... OK  
-- Checking if https://github.com is reachable ... OK  
-- Checking if requested OpenShift version 'v3.11.0' is valid ... OK  
-- Checking if requested OpenShift version 'v3.11.0' is supported ... OK  
-- Checking if requested hypervisor 'virtualbox' is supported on this platform ... OK  
-- Checking if VirtualBox is installed ... OK  
-- Checking the ISO URL ... OK  
-- Downloading OpenShift binary 'oc' version 'v3.11.0'  
53.59 MiB / 53.59 MiB [=============================] 100.00% 0s-- Downloading OpenShift v3.11.0 checksums ... OK  
-- Checking if provided oc flags are supported ... OK  
-- Starting the OpenShift cluster using 'virtualbox' hypervisor ...  
-- Minishift VM will be configured with ...  
Memory:    4 GB  
vCPUs :    2  
Disk size: 20 GB  

Downloading ISO 'https://github.com/minishift/minishift-centos-iso/releases/download/v1.17.0/minishift-centos7.iso'  
375.00 MiB / 375.00 MiB [========================] 100.00% 0s  
-- Starting Minishift VM ................................................... OK  
-- Checking for IP address ... OK  
-- Checking for nameservers ... OK  
-- Checking if external host is reachable from the Minishift VM ...  
Pinging 8.8.8.8 ... OK  
-- Checking HTTP connectivity from the VM ...  
Retrieving http://minishift.io/index.html ... OK  
-- Checking if persistent storage volume is mounted ... OK  
-- Checking available disk space ... 1% used OK  
-- Writing current configuration for static assignment of IP address ... OK  
Importing 'openshift/origin-control-plane:v3.11.0' . CACHE MISS  
Importing 'openshift/origin-docker-registry:v3.11.0' . CACHE MISS  
Importing 'openshift/origin-haproxy-router:v3.11.0' . CACHE MISS  
-- OpenShift cluster will be configured with ...  
Version: v3.11.0  
-- Pulling the OpenShift Container Image ..................... OK  
-- Copying oc binary from the OpenShift container image to VM ... OK  
-- Starting OpenShift cluster .....................  

Login to server ...
Creating initial project "myproject" ...
Server Information ...
OpenShift server started.

The server is accessible via web console at:
https://192.168.99.100:8443/console

You are logged in as:
User:     developer
Password: <any value>

To login as administrator:
oc login -u system:admin

-- Exporting of OpenShift images is occuring in background process with pid 15376.
