## Some Degreed Course
```
https://github.com/wardviaene/kubernetes-course  
```
# Below is for Huawei cloud internal resource (seems version is old)
# 1.备份/etc/apt/sources.list.d/kubernetes.list文件:
cp /etc/apt/sources.list.d/kubernetes.list /etc/apt/sources.list.d/kubernetes.list.bak

# 2、修改/etc/apt/sources.list.d/kubernetes.list文件：
cat <<EOF > /etc/apt/sources.list.d/kubernetes.list
deb https://repo.huaweicloud.com/kubernetes/apt/ kubernetes-xenial main
EOF

# 3、添加kubernetes的key
curl -s https://repo.huaweicloud.com/kubernetes/apt/doc/apt-key.gpg | sudo apt-key add -

# 4、更新索引文件并安装kubernetes
sudo apt update  
sudo apt install -y kubeadm kubelet kubectl

# Above is from HuaWei cloud, not work(unknown reason)

# Install kubectl with curl
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"

# Download kubectl checksum file
curl -LO "https://dl.k8s.io/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl.sha256"

# Validate the kubectl against the checksum file
echo "$(cat kubectl.sha256)  kubectl" | sha256sum --check

## if result is 'OK', that means it's valid version

# Install kubectl
sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl

# Check kubectl version
kubectl version --client  
Client Version: version.Info{Major:"1", Minor:"23", GitVersion:"v1.23.5", GitCommit:"c285e781331a3785a7f436042c65c5641ce8a9e9", 
GitTreeState:"clean", BuildDate:"2022-03-16T15:58:47Z", 
GoVersion:"go1.17.8", Compiler:"gc", Platform:"linux/amd64"}

# Install kubeadm with curl
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubeadm"

# Download kubeadm checksum file
curl -LO "https://dl.k8s.io/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubeadm.sha256"

# Validate the kubeadm against the checksum file
echo "$(cat kubeadm.sha256)  kubeadm" | sha256sum --check

# Install kubeadm
sudo install -o root -g root -m 0755 kubeadm /usr/local/bin/kubeadm
# Check kubeadm version
kubeadm version  
kubeadm version: &version.Info{Major:"1", Minor:"23", GitVersion:"v1.23.5", GitCommit:"c285e781331a3785a7f436042c65c5641ce8a9e9", 
GitTreeState:"clean", BuildDate:"2022-03-16T15:57:37Z", 
GoVersion:"go1.17.8", Compiler:"gc", Platform:"linux/amd64"}

# Install kubelet with curl
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubelet"
# Download kubelet checksum file
curl -LO "https://dl.k8s.io/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubelet.sha256"
# Validate the kubelet against the checksum file
echo "$(cat kubelet.sha256)  kubelet" | sha256sum --check
# Install kubelet
sudo install -o root -g root -m 0755 kubelet /usr/local/bin/kubelet

# Install Minikube
## Download Minikube from Github(latesst version ,.exe file)
https://github.com/kubernetes/minikube/releases  
## start up Minikube
minikube.exe start

## installing

✨  自动选择 virtualbox 驱动  
💿  正在下载 VM boot image...  
> minikube-v1.25.2.iso.sha256: 65 B / 65 B [-------------] 100.00% ? p/s 0s  
> minikube-v1.25.2.iso: 237.06 MiB / 237.06 MiB [ 100.00% 16.48 MiB p/s 15s  
👍  Starting control plane node minikube in cluster minikube  
💾  Downloading Kubernetes v1.23.3 preload ...  
> preloaded-images-k8s-v17-v1...: 505.68 MiB / 505.68 MiB  100.00% 14.62 Mi  
🔥  Creating virtualbox VM (CPUs=2, Memory=2200MB, Disk=20000MB) ...  
❗  This VM is having trouble accessing https://k8s.gcr.io  
💡  To pull new external images, you may need to configure a proxy: https://minikube.sigs.k8s.io/docs/reference/networking/proxy/     
🐳  正在 Docker 20.10.12 中准备 Kubernetes v1.23.3…  
▪ kubelet.housekeeping-interval=5m  
▪ Generating certificates and keys ...  
▪ Booting up control plane ...  
▪ Configuring RBAC rules ...  
▪ Using image gcr.io/k8s-minikube/storage-provisioner:v5  
🔎  Verifying Kubernetes components...  
🌟  Enabled addons: storage-provisioner, default-storageclass  
💡  kubectl not found. If you need it, try: 'minikube kubectl -- get pods -A'  
🏄  Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default  

# Startup dashboard
minikube.exe dashboard  

🤔  正在验证 dashboard 运行情况 ...  
🚀  Launching proxy ...  
🤔  正在验证 proxy 运行状况 ...  
🎉  Opening http://127.0.0.1:59503/api/v1/namespaces/kubernetes-dashboard/services/http:kubernetes-dashboard:/proxy/ in your default browser...  
![avatar](/resources/pic/k8s_dashboard.png)

# Login to Minikube VM
minikube.exe ssh  
_             _
_         _ ( )           ( )
___ ___  (_)  ___  (_)| |/')  _   _ | |_      __
/' _ ` _ `\| |/' _ `\| || , <  ( ) ( )| '_`\  /'__`\
| ( ) ( ) || || ( ) || || |\`\ | (_) || |_) )(  ___/
(_) (_) (_)(_)(_) (_)(_)(_) (_)`\___/'(_,__/'`\____)
  
$ pwd  
/home/docker  


# Install kubectl
https://kubernetes.io/docs/tasks/tools/install-kubectl-windows/

# Download exe file, (curl command is slow, not recommend to use it)
## check version
C:\>kubectl.exe version --client  
Client Version: version.Info{Major:"1", Minor:"23", GitVersion:"v1.23.0", GitCommit:"ab69524f795c42094a6630298ff53f3c3ebab7f4", GitTreeState:"clean", BuildDate:"2021-12-07T18:16:20Z", GoVersion:"go1.17.3", Compiler:"gc", Platform:"windows/amd64"}

## verify kubectl configuration
C:\>kubectl.exe cluster-info  
Kubernetes control plane is running at https://192.168.59.100:8443  
CoreDNS is running at https://192.168.59.100:8443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy  

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.  

## Create sample deployment
C:\>kubectl.exe create deployment hello-minikube --image=k8s.gcr.io/echoserver:1.4  
deployment.apps/hello-minikube created  
## after that you go to minikube dashboard, you can see deployment is in there
![avatar](/resources/pic/k8s_dashboard_sample_deployment.png)

## Exposing a service as a NodePort
C:\>kubectl.exe expose deployment hello-minikube --type=NodePort --port=9098                                                   
service/hello-minikube exposed

## make hello-minikube as service
C:\>minikube.exe service hello-minikube  
as hello-minikube deployment failed in K8s, so will not show anything here  
in normal, if success, it will show up the service URL, like 192.168.64.8:31626  

## deploy service with existing image
kubectl.exe create -f skybook.yml  

apiVersion: v1  
kind: Pod  
metadata:  
name: skybook-k8s  
labels:  
app: skybook  
spec:  
containers:  
- name: skybook-k8s  
  image: zheqiushui/skybook  
  ports:  
    - containerPort: 3000  

# Check pod status
C:>kubectl.exe get pod  
NAME                  READY   STATUS    RESTARTS   AGE  
skybook-k8s           1/1     Running   0          16m  

# Check pod details
kubectl.exe describe pod skybook-k8s  

Name:         skybook-k8s  
Namespace:    default  
Priority:     0  
Node:         minikube/192.168.59.100  
Start Time:   Fri, 08 Apr 2022 22:34:00 +0800  
Labels:       app=skybook  
Annotations:  <none>  
Status:       Running  
IP:           172.17.0.6  
IPs:  
IP:  172.17.0.6  
Containers:  
skybook-k8s:  
Container ID:   docker://4baee72884b918ff51733278f79aecc8e6a8725c95cfb73bf500c06b1681068d  
Image:          zheqiushui/skybook  
Image ID:       docker-pullable://zheqiushui/skybook@sha256:58326349976100867470b2cd161f4b18687634cd8ca169cac148b5ecaf8b4489     
Port:           3000/TCP  
Host Port:      0/TCP  
State:          Running  
Started:      Fri, 08 Apr 2022 22:36:06 +0800  
Ready:          True  
Restart Count:  0  
Environment:    <none>  
Mounts:  
/var/run/secrets/kubernetes.io/serviceaccount from kube-api-access-hdwgf (ro)  
Conditions:  
Type              Status  
Initialized       True  
Ready             True  
ContainersReady   True  
PodScheduled      True  
Volumes:  
kube-api-access-hdwgf:  
Type:                    Projected (a volume that contains injected data from multiple sources)  
TokenExpirationSeconds:  3607  
ConfigMapName:           kube-root-ca.crt  
ConfigMapOptional:       <nil>  
DownwardAPI:             true  
QoS Class:                   BestEffort  
Node-Selectors:              <none>  
Tolerations:                 node.kubernetes.io/not-ready:NoExecute op=Exists for 300s  
node.kubernetes.io/unreachable:NoExecute op=Exists for 300s  
Events:  
Type    Reason     Age   From               Message  
  ----    ------     ----  ----               -------
Normal  Scheduled  18m   default-scheduler  Successfully assigned default/skybook-k8s to minikube  
Normal  Pulling    18m   kubelet            Pulling image "zheqiushui/skybook"  
Normal  Pulled     16m   kubelet            Successfully pulled image "zheqiushui/skybook" in 2m4.345016629s  
Normal  Created    16m   kubelet            Created container skybook-k8s  
Normal  Started    16m   kubelet            Started container skybook-k8s  

# Port forward to start up service
kubectl.exe port-forward skybook-k8s 8188:3000  

# Expose pod
kubectl.exe expose pod skybook-k8s --type=NodePort  --name skybook-service
service/skybook-service exposed  

# Get service url
minikube.exe service skybook-service --url  
http://192.168.59.100:30645  

# Get all running service
kubectl.exe get service  
NAME              TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE  
hello-minikube    NodePort    10.107.56.211   <none>        9098:32384/TCP   4d  
kubernetes        ClusterIP   10.96.0.1       <none>        443/TCP          4d1h  
skybook-service   NodePort    10.97.189.150   <none>        3000:30645/TCP   3m19s  

# Check pods internal folders
kubectl.exe exec skybook-k8s -- ls /  
run  
sbin  
skybook.jar  
srv  
sys  
tmp  

# Show service detail
kubectl.exe describe service skybook-service  
Name:                     skybook-service  
Namespace:                default  
Labels:                   app=skybook  
Annotations:              <none>  
Selector:                 app=skybook  
Type:                     NodePort  
IP Family Policy:         SingleStack  
IP Families:              IPv4  
IP:                       10.97.189.150  
IPs:                      10.97.189.150  
Port:                     <unset>  3000/TCP  
TargetPort:               3000/TCP  
NodePort:                 <unset>  30645/TCP  
Endpoints:                172.17.0.2:3000  
Session Affinity:         None  
External Traffic Policy:  Cluster  
Events:                   <none>  

# Generate secrets using external files
``
C:\devops>echo -n "root" > ./username.txt  
C:\devops>echo -n "password" > ./password.txt
C:\devops>kubectl.exe create secret generic db-user-pass --from-file=.\username.txt --from-file=.\password.txt  
secret/db-user-pass created  
``
# Create configmap
``
C:\devops>kubectl.exe create configmap nginx-config --from-file=reverseproxy.conf   
configmap/nginx-config created
``
# Check configmap
``
C:\devops>kubectl.exe get configmap  
NAME               DATA   AGE  
kube-root-ca.crt   1      11d  
nginx-config       1      19s  
``
# Get specific configmap content
``
C:\devops>kubectl.exe get configmap nginx-config -o yaml  
apiVersion: v1  
data:  
    reverseproxy.conf: "server {      
    listen      80;  
    server_name localhost;  
...  
kind: ConfigMap  
metadata:  
creationTimestamp: "2022-04-18T14:22:12Z"  
name: nginx-config  
namespace: default  
resourceVersion: "15167"  
uid: 6e057c6c-d51b-4fb7-82ad-88b0b8f1d9b1  
``