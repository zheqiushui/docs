#安装sudo
su  
apt-get install sudo

#安装yum
sudo apt-get update  
sudo apt-get install build-essential  
sudo apt-get install yum

### 安装maven
sudo apt-get install maven

### 安装JDK
sudo apt-get update  
sudo apt-get install default-jdk  

#安装Jenkins
###获取docker image
docker pull jenkins/jenkins
### 创建数据目录 (jenkins root folder)
mkdir /data/devops/jenkins

### 启动jenkins
### -v /var/run/docker.sock:/var/run/docker.sock 使Jenkins Docker 与容器外(系统)docker一样
docker run -d -p 8080:8080 -p 50000:50000 -v /data/devops/jenkins:/var/jenkins_home -v /etc/localtime:/etc/localtime -v /var/run/docker.sock:/var/run/docker.sock --name jenkins-server jenkins/jenkins

### 进入container 内部
docker exec -it 7ae542c81087 bash  

# Build Docker Image
## clone source code from BitBucket
git clone https://ZheQiushui@bitbucket.org/zheqiushui/skybook.git  

## maven build
mvn clean install  

## execute build
docker build -t skybook:v1.0 .  

## After build, you can see image is in local repo
root:/opt/project/skybook# docker image ls  
REPOSITORY TAG   IMAGE ID     CREATED              SIZE  
skybook    v1.0  8ab65a2c93ed About a minute ago   137MB  

## run docker image to start up SkyBook 
#### -d to make it run in daemon mode
#### 8081:8080 -> tomcat default to open 8080, we map 8080 to 8081 external of container
docker run -d -it -p 8081:8080 skybook:v1.0  

# Push image to Docker Hub
## login to Docker Hub
docker login -u zheqiushui  
## tag local image (zheqiushui/skybook:v1.0 is the remote image name)
docker tag skybook:v1.0 zheqiushui/skybook:v1.0  
## push local image to remote (Docker Hub)
docker push zheqiushui/skybook:v1.0  